package com.sportech.common.model;

public enum CardType {
	Thoroughbred,
	Harness,
	GreyHound,
	VirtualDog,
	VirtualHorse,
	VirtualHarness
}
