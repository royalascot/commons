package com.sportech.common.model;

public interface RunnerContainer {

    public String getPoolIdList();
    public void setPoolIdList(String poolIdList);

}
