package com.sportech.common.model.message;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonCreator;


public class ServiceCommand implements Serializable {
    
    private static final long serialVersionUID = -1L;
       
    private String command;
    private String body;
    
    @JsonCreator
    public ServiceCommand() {
        
    }
    
    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
   
}
