package com.sportech.common.model;

import org.apache.commons.lang3.StringUtils;


public enum PoolType {   
    
    WIN(1,"Win","WIN",1,0,0,0,1,1,1),
    
    PLC(2,"Place","PLC",2,0,0,0,1,1,1),
    
    SHW(3,"Show","SHW",3,0,0,0,1,1,1),
    
    EXA(4,"Exacta","EXA",4,0,0,0,2,1,1),
    
    QNL(5,"Quinella","QNL",5,0,0,0,2,1,1),
    
    TRI(6,"Trifecta","TRI",6,0,0,0,3,1,1),
    
    DBL(7,"Double","DBL",7,1,0,0,2,2,2),
    
    PK3(8,"Pick 3","PK3",8,1,0,0,3,3,3),
    
    PK4(9,"Pick 4","PK4",9,1,0,0,4,4,4),
    
    PK5(10,"Pick 5","PK5",10,1,0,0,5,5,5),
    
    PK6(11,"Pick 6","PK6",11,1,0,0,6,6,6),
    
    PK7(12,"Pick 7","PK7",12,1,0,0,7,7,7),
    
    PK8(13,"Pick 8","PK8",13,1,0,0,8,8,8),
    
    PK9(14,"Pick 9","PK9",14,1,0,0,9,9,9),
    
    P10(15,"Pick 10","P10",15,1,0,0,10,10,10),
    
    TQN(16,"Twin Quinella","TQN",16,1,0,1,2,2,2),
    
    DQL(17,"Double Quinella","DQL",17,1,0,0,4,2,2),
    
    DEX(18,"Double Exacta","DEX",18,1,0,0,4,2,2),
    
    TTR(19,"Twin Trifecta","TTR",19,1,0,1,3,2,2),
    
    TET(20,"Exact Tri","TET",20,1,0,1,3,2,2),
    
    SFC(21,"Superfecta","SFC",21,0,0,0,4,1,1),
    
    BTS(22,"Tri Super","BTS",22,1,0,1,3,2,2),
    
    PPT(23,"PPT PER","PPT",23,1,0,0,7,3,3),
    
    TSS(24,"Super Super","TSS",24,1,0,1,4,2,2),
    
    OMN(25,"Omni","OMN",25,0,0,0,2,1,1),
    
    RNG(26,"Racingo","RNG",26,1,0,0,9,3,3),
    
    CH6(27,"Choose 6","CH6",27,1,0,0,6,6,6),
    
    E5N(28,"PentaFecta","E5N",28,0,0,0,5,1,1),
    
    OTT(29,"1-2-3","OTT",29,1,1,0,4,4,10),
    
    WXN(30,"Multiple","WXN",30,1,1,1,2,2,15),
    
    VPK(31,"Variable Pick","VPK",31,1,1,0,2,2,15),
    
    PKX(32,"Scoop 6","PKX",32,1,0,0,6,6,6),
    
    GSL(40,"Grand Slam","GSL",9,1,0,0,4,4,4);
    
    private PoolType(int id, String name, String code, int toteId, int multiRace, int variLeg, int exchange, int legCount, int raceCount, int maxRaceCount) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.toteId = toteId;
        this.multiRace = (multiRace > 0);
        this.variLeg = (variLeg > 0);
        this.exchange = (exchange > 0);
        this.legCount = legCount;
        this.raceCount = raceCount;
        this.maxRaceCount = maxRaceCount;
    }
    
    private final int id;
    
    private final String code;
    private final String name;
    
    private final int toteId;
    
    private final boolean multiRace;
    
    private final boolean variLeg;
    
    private final boolean exchange;
    
    private final int legCount;
    
    private final int raceCount;
    
    private final int maxRaceCount;

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public int getToteId() {
        return toteId;
    }

    public boolean isMultiRace() {
        return multiRace;
    }

    public boolean isVariLeg() {
        return variLeg;
    }

    public boolean isExchange() {
        return exchange;
    }

    public int getLegCount() {
        return legCount;
    }

    public int getRaceCount() {
        return raceCount;
    }

    public int getMaxRaceCount() {
        return maxRaceCount;
    }

    public static PoolType fromId(int id) {
        for (PoolType pt : PoolType.values()) {
            if (pt.getId() == id) {
                return pt;
            }
        }
        return null;
    }
    
    public static PoolType fromCode(String code) {
        for (PoolType pt : PoolType.values()) {
            if (StringUtils.equalsIgnoreCase(pt.getCode(), code)) {
                return pt;
            }
        }
        return null;
    }
   
    public static PoolType fromName(String name) {
        for (PoolType pt : PoolType.values()) {
            if (StringUtils.equalsIgnoreCase(pt.getName(), name)) {
                return pt;
            }
        }
        return null;
    }
    
}
