package com.sportech.common.model.entity;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

@SuppressWarnings("serial")
public class PoolLeg implements Serializable {
	
	@JsonCreator
	public PoolLeg() {
		
	}
	
	@JsonProperty("L")
	private Integer legNumber;
	
	@JsonProperty("P")
	private Integer[] poolIds;

	public Integer getLegNumber() {
		return legNumber;
	}

	public void setLegNumber(Integer legNumber) {
		this.legNumber = legNumber;
	}

	public Integer[] getPoolIds() {
		return poolIds;
	}

	public void setPoolIds(Integer[] poolIds) {
		this.poolIds = poolIds;
	}
	
}
