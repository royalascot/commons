package com.sportech.common.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sportech.common.model.CardType;
import com.sportech.common.model.PerformanceType;

@Entity
@Table(name = "event_card",

indexes = {

@Index(name = "card_index_date_code", columnList = "card_date,source_id", unique = true),

@Index(name = "card_index_lastupdate", columnList = "last_update", unique = false)

})
public class EventCard implements Serializable {

	private static final long serialVersionUID = -1L;

	private Long id;

	private String name;
	private String meetingType;
	private String eventCode;
	private String provider;
	private String cardDate;
	private String startTime;
	private CardType cardType;
	private PerformanceType perfType;
	private Date firstRaceTime;
	private String description;
	private String meetingCode;
	private String racingStatus;
	private String coverageCode;
	private String subCode;
	private String going;

	private byte[] cardData;
	private String messageId;

	private String sourceId;
	private String sourceType;
	private Date importTime;
	private String status;
	private String filename;
	private Long checksum;

	private String countryCode;
	private String trackCode;
	private String trackName;
	private String toteCode;

	private Set<EventRace> races = new HashSet<EventRace>();
	private Set<EventPool> pools = new HashSet<EventPool>();

	private Integer raceCount;

	private CardStatus processStatus;

	private Long lastUpdate;
	
	public EventCard() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", length = 100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "coverage_code", length = 20)
	public String getCoverageCode() {
		return coverageCode;
	}

	public void setCoverageCode(String coverageCode) {
		this.coverageCode = coverageCode;
	}

	@Column(name = "going", length = 20)
	public String getGoing() {
		return going;
	}

	public void setGoing(String going) {
		this.going = going;
	}

	@Column(name = "meeting_type", length = 50)
	public String getMeetingType() {
		return meetingType;
	}

	public void setMeetingType(String meetingType) {
		this.meetingType = meetingType;
	}

	@Column(name = "racing_status", length = 50)
	public String getRacingStatus() {
		return racingStatus;
	}

	public void setRacingStatus(String racingStatus) {
		this.racingStatus = racingStatus;
	}

	@Column(name = "import_time")
	public Date getImportTime() {
		return importTime;
	}

	public void setImportTime(Date importTime) {
		this.importTime = importTime;
	}

	@Column(name = "status", length = 50)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "filename", length = 150)
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Column(name = "checksum")
	public Long getChecksum() {
		return checksum;
	}

	public void setChecksum(Long checksum) {
		this.checksum = checksum;
	}

	@Column(name = "meeting_code", length = 50)
	public String getMeetingCode() {
		return meetingCode;
	}

	public void setMeetingCode(String meetingCode) {
		this.meetingCode = meetingCode;
	}

	@Column(name = "event_code", length = 50)
	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	@Column(name = "provider", length = 100)
	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	@Column(name = "card_date", length = 20)
	public String getCardDate() {
		return cardDate;
	}

	public void setCardDate(String cardDate) {
		this.cardDate = cardDate;
	}

	@Column(name = "start_time", length = 20)
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@Column(name = "perf_type")
	public PerformanceType getPerfType() {
		return perfType;
	}

	public void setPerfType(PerformanceType perfType) {
		this.perfType = perfType;
	}

	@Column(name = "card_type")
	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	@Column(name = "first_race_time")
	public Date getFirstRaceTime() {
		return firstRaceTime;
	}

	public void setFirstRaceTime(Date firstRaceTime) {
		this.firstRaceTime = firstRaceTime;
	}

	@Column(name = "description", length = 100)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "sub_code", length = 20)
	public String getSubCode() {
		return subCode;
	}

	public void setSubCode(String subCode) {
		this.subCode = subCode;
	}

	@Lob
	@Column(name = "card_data")
	public byte[] getCardData() {
		return cardData;
	}

	public void setCardData(byte[] cardData) {
		this.cardData = cardData;
	}

	@OneToMany(mappedBy = "card", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	public Set<EventRace> getRaces() {
		return races;
	}

	public void setRaces(Set<EventRace> races) {
		this.races = races;
	}

	@OneToMany(mappedBy = "card", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	public Set<EventPool> getPools() {
		return pools;
	}

	public void setPools(Set<EventPool> pools) {
		this.pools = pools;
	}

	@Column(name = "source_id", length = 50)
	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	@Column(name = "source_type", length = 50)
	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	@Column(name = "process_status")
	public CardStatus getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(CardStatus processStatus) {
		this.processStatus = processStatus;
	}

	@Column(name = "message_id", length = 100)
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	@Column(name = "track_code", length = 20)
	public String getTrackCode() {
		return trackCode;
	}

	public void setTrackCode(String trackCode) {
		this.trackCode = trackCode;
	}

	@Column(name = "track_name", length = 50)
	public String getTrackName() {
		return trackName;
	}

	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}

	@Column(name = "tote_code", length = 20)
	public String getToteCode() {
		return toteCode;
	}

	public void setToteCode(String toteCode) {
		this.toteCode = toteCode;
	}

	@Column(name = "country_code", length = 20)
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Column(name = "race_count")
	public Integer getRaceCount() {
		return raceCount;
	}

	public void setRaceCount(Integer raceCount) {
		this.raceCount = raceCount;
	}

	@Column(name = "last_update")
	public Long getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cardDate == null) ? 0 : cardDate.hashCode());
		result = prime * result + ((sourceId == null) ? 0 : sourceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventCard other = (EventCard) obj;
		if (cardDate == null) {
			if (other.cardDate != null)
				return false;
		} else if (!cardDate.equals(other.cardDate))
			return false;
		if (sourceId == null) {
			if (other.sourceId != null)
				return false;
		} else if (!sourceId.equals(other.sourceId))
			return false;
		return true;
	}

}
