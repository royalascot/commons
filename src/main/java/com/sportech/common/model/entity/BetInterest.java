package com.sportech.common.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bet_interest")
public class BetInterest implements Serializable {

	private static final long serialVersionUID = -1L;

	private Long id;

	private String name;
	private String shortName;

	private Long position;
	private String programNumber;
	private String jockeyName;
	private String trainer;

	private String weightCarried;
	private String weightUnits;
	private Integer weightStones;
	private Integer weightPounds;

	private String status;
	private String coupleIndicator;
	private String mornlineOdds;
	private String silk;
	private Boolean scratched;
	private Boolean alsoEligible;

	private String sourceId;
	private String sourceType;

	private EventRace race;

	private BigDecimal dec;
	private String fract;
	private Long marketNum;
	private String marketType;
	private Date priceTime;
	private Long priceTimestamp;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "race_id")
	public EventRace getRace() {
		return race;
	}

	public void setRace(EventRace race) {
		this.race = race;
	}

	@Column(name = "name", length = 100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "position")
	public Long getPosition() {
		return position;
	}

	public void setPosition(Long position) {
		this.position = position;
	}

	@Column(name = "program_number", length = 30)
	public String getProgramNumber() {
		return programNumber;
	}

	public void setProgramNumber(String programNumber) {
		this.programNumber = programNumber;
	}

	@Column(name = "jockey_name", length = 100)
	public String getJockeyName() {
		return jockeyName;
	}

	public void setJockeyName(String jockeyName) {
		this.jockeyName = jockeyName;
	}

	@Column(name = "trainer", length = 50)
	public String getTrainer() {
		return trainer;
	}

	public void setTrainer(String trainer) {
		this.trainer = trainer;
	}

	@Column(name = "weight_carried", length = 50)
	public String getWeightCarried() {
		return weightCarried;
	}

	public void setWeightCarried(String weightCarried) {
		this.weightCarried = weightCarried;
	}

	@Column(name = "weight_units", length = 50)
	public String getWeightUnits() {
		return weightUnits;
	}

	public void setWeightUnits(String weightUnits) {
		this.weightUnits = weightUnits;
	}

	@Column(name = "couple_indicator", length = 20)
	public String getCoupleIndicator() {
		return coupleIndicator;
	}

	public void setCoupleIndicator(String coupleIndicator) {
		this.coupleIndicator = coupleIndicator;
	}

	@Column(name = "mornline_odds", length = 150)
	public String getMornlineOdds() {
		return mornlineOdds;
	}

	public void setMornlineOdds(String mornlineOdds) {
		this.mornlineOdds = mornlineOdds;
	}

	@Column(name = "silk", length = 100)
	public String getSilk() {
		return silk;
	}

	public void setSilk(String silk) {
		this.silk = silk;
	}

	@Column(name = "scratched")
	public Boolean getScratched() {
		return scratched;
	}

	public void setScratched(Boolean scratched) {
		this.scratched = scratched;
	}

	@Column(name = "also_eligible")
	public Boolean getAlsoEligible() {
		return alsoEligible;
	}

	public void setAlsoEligible(Boolean alsoEligible) {
		this.alsoEligible = alsoEligible;
	}

	@Column(name = "source_id", length = 50)
	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	@Column(name = "source_type", length = 50)
	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	@Column(name = "weight_stones")
	public Integer getWeightStones() {
		return weightStones;
	}

	public void setWeightStones(Integer weightStones) {
		this.weightStones = weightStones;
	}

	@Column(name = "weight_pounds")
	public Integer getWeightPounds() {
		return weightPounds;
	}

	public void setWeightPounds(Integer weightPounds) {
		this.weightPounds = weightPounds;
	}

	@Column(name = "short_name", length = 50)
	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	// Prices
	@Column(name = "dec_price")
	public BigDecimal getDec() {
		return dec;
	}

	public void setDec(BigDecimal dec) {
		this.dec = dec;
	}

	@Column(name = "fract_price", length = 20)
	public String getFract() {
		return fract;
	}

	public void setFract(String fract) {
		this.fract = fract;
	}

	@Column(name = "market_num")
	public Long getMarketNum() {
		return marketNum;
	}

	public void setMarketNum(Long marketNum) {
		this.marketNum = marketNum;
	}

	@Column(name = "market_type", length = 20)
	public String getMarketType() {
		return marketType;
	}

	public void setMarketType(String marketType) {
		this.marketType = marketType;
	}

	@Column(name = "price_time")
	public Date getPriceTime() {
		return priceTime;
	}

	public void setPriceTime(Date priceTime) {
		this.priceTime = priceTime;
	}

	@Column(name = "price_timestamp")
	public Long getPriceTimestamp() {
		return priceTimestamp;
	}

	public void setPriceTimestamp(Long priceTimestamp) {
		this.priceTimestamp = priceTimestamp;
	}
	
	@Column(name = "status", length = 20)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BetInterest other = (BetInterest) obj;
		if (position != null && other.position != null) {
			if (!position.equals(other.position)) {
				return false;
			}
		}
		return true;
	}

}
