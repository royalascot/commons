package com.sportech.common.model.entity;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sportech.common.model.PoolType;
import com.sportech.common.util.ListHelper;

@Entity
@Table(name = "event_pool")
public class EventPool implements Serializable {

	private static final long serialVersionUID = 1252846686542064117L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "pool_type")
	private PoolType poolType;

	@ManyToOne
	@JoinColumn(name = "card_id")
	private EventCard card;

	@Column(length = 50)
	private String code;

	@Column(name="pool_name", length = 50)
	private String poolName;

	@Column(name="race_list", length = 150)
	private String raceNumbers;

	@Transient
	private Set<Long> races = null;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public EventCard getCard() {
		return card;
	}

	public void setCard(EventCard eventData) {
		this.card = eventData;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPoolName() {
		return poolName;
	}

	public void setPoolName(String poolName) {
		this.poolName = poolName;
	}

	public String getRaceNumbers() {
		return raceNumbers;
	}

	public void setRaceNumbers(String raceNumbers) {
		this.raceNumbers = raceNumbers;
	}

	public PoolType getPoolType() {
		return poolType;
	}

	public void setPoolType(PoolType poolType) {
		this.poolType = poolType;
	}

	@Transient
	public Set<Long> getRaces() {
		if (races == null) {
			races = new TreeSet<Long>();
			Set<Long> items = ListHelper.parseList(raceNumbers);
			races.addAll(items);
		}
		return races;
	}

	public void setRaces(Set<Long> races) {
		this.races = races;
		refresh();
	}

	public void refresh() {
		setRaceNumbers(ListHelper.build(races));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getPoolType() == null) ? 0 : getPoolType().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventPool other = (EventPool) obj;
		if (getPoolType() == null) {
			if (other.getPoolType() != null) {
				return false;
			}
		} else if (!getPoolType().equals(other.getPoolType())) {
			return false;
		}
		return true;
	}

}
