package com.sportech.common.model.entity;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

@SuppressWarnings("serial")
public class PoolList implements Serializable {
	
	@JsonCreator
	public PoolList() {
		
	}
	
	@JsonProperty("S")
	private List<PoolLeg> legs;

	public List<PoolLeg> getLegs() {
		return legs;
	}

	public void setLegs(List<PoolLeg> legs) {
		this.legs = legs;
	}

}
