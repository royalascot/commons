package com.sportech.common.model.entity;

public enum CardStatus {
	New,
	RacesDefined,
	PoolsDefined,
	Update,
	Processed
}
