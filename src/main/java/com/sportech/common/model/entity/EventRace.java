package com.sportech.common.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.sportech.common.model.PerformanceType;
import com.sportech.common.model.RunnerContainer;

@Entity
@Table(name = "event_race")
public class EventRace implements Serializable, RunnerContainer {

	private static final long serialVersionUID = -1L;

	private Long id;
	private Long number;
	private Date postTime;
	private String distance;
	private PerformanceType perfType;
	private String status;
	private String progressCode;
	private String going;
	private Date offTime;
	private String surface;
	private String grade;
	private Boolean handicap;
	private String coverageCode;
	private String breed;
	private String course;
	private String raceType;
	private String raceNameShort;
	private String raceNameLong;
	private String wagerText;
	private String poolIdList;

	private Integer runnersCount;
	private Integer placesExpected;
	private Integer eachWayPlaces;

	private BigDecimal purse;

	private Boolean enabled;

	private Set<BetInterest> runners = new HashSet<BetInterest>();

	private EventCard card;

	private Long eventId;

	private String sourceId;
	private String sourceType;

	private String raceCode;
	private String distanceText;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "card_id")
	public EventCard getCard() {
		return card;
	}

	public void setCard(EventCard eventData) {
		this.card = eventData;
	}

	@Column(name = "number")
	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	@Column(name = "runners_count")
	public Integer getRunnersCount() {
		return runnersCount;
	}

	public void setRunnersCount(Integer runnersCount) {
		this.runnersCount = runnersCount;
	}

	@Column(name = "post_time")
	public Date getPostTime() {
		return postTime;
	}

	public void setPostTime(Date postTime) {
		this.postTime = postTime;
	}

	@Column(name = "distance", length = 100)
	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	@Column(name = "breed", length = 50)
	public String getBreed() {
		return breed;
	}

	public void setBreed(String breed) {
		this.breed = breed;
	}

	@Column(name = "course", length = 50)
	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	@Column(name = "race_type", length = 50)
	public String getRaceType() {
		return raceType;
	}

	public void setRaceType(String raceType) {
		this.raceType = raceType;
	}

	@Column(name = "race_name_short", length = 150)
	public String getRaceNameShort() {
		return raceNameShort;
	}

	public void setRaceNameShort(String raceNameShort) {
		this.raceNameShort = raceNameShort;
	}

	@Column(name = "race_name_long", length = 150)
	public String getRaceNameLong() {
		return raceNameLong;
	}

	public void setRaceNameLong(String raceNameLong) {
		this.raceNameLong = raceNameLong;
	}

	@Column(name = "place_expected")
	public Integer getPlacesExpected() {
		return placesExpected;
	}

	public void setPlacesExpected(Integer placesExpected) {
		this.placesExpected = placesExpected;
	}

	@Column(name = "each_way_places")
	public Integer getEachWayPlaces() {
		return eachWayPlaces;
	}

	public void setEachWayPlaces(Integer eachWayPlaces) {
		this.eachWayPlaces = eachWayPlaces;
	}

	@Column(name = "status", length = 20)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "progress_code", length = 20)
	public String getProgressCode() {
		return progressCode;
	}

	public void setProgressCode(String progressCode) {
		this.progressCode = progressCode;
	}

	@Column(name = "going", length = 20)
	public String getGoing() {
		return going;
	}

	public void setGoing(String going) {
		this.going = going;
	}

	@Column(name = "off_time")
	public Date getOffTime() {
		return offTime;
	}

	public void setOffTime(Date offTime) {
		this.offTime = offTime;
	}

	@Column(name = "surface", length = 20)
	public String getSurface() {
		return surface;
	}

	public void setSurface(String surface) {
		this.surface = surface;
	}

	@Column(name = "grade", length = 20)
	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	@Column(name = "coverage_code", length = 20)
	public String getCoverageCode() {
		return coverageCode;
	}

	public void setCoverageCode(String coverageCode) {
		this.coverageCode = coverageCode;
	}

	@Column(name = "purse")
	public BigDecimal getPurse() {
		return purse;
	}

	public void setPurse(BigDecimal purse) {
		this.purse = purse;
	}

	@Column(name = "enabled")
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	@OneToMany(mappedBy = "race", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	public Set<BetInterest> getRunners() {
		return runners;
	}

	public void setRunners(Set<BetInterest> runners) {
		this.runners = runners;
	}

	@Column(name = "wager_text", length = 150)
	public String getWagerText() {
		return wagerText;
	}

	public void setWagerText(String wagerText) {
		this.wagerText = wagerText;
	}

	@Column(name = "pool_id_list", length = 150)
	public String getPoolIdList() {
		return poolIdList;
	}

	public void setPoolIdList(String poolIdList) {
		this.poolIdList = poolIdList;
	}

	@Column(name = "event_id")
	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	@Column(name = "perf_type")
	public PerformanceType getPerfType() {
		return perfType;
	}

	public void setPerfType(PerformanceType perfType) {
		this.perfType = perfType;
	}

	@Column(name = "handicap")
	public Boolean getHandicap() {
		return handicap;
	}

	public void setHandicap(Boolean handicap) {
		this.handicap = handicap;
	}

	@Column(name = "source_id", length = 50)
	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	@Column(name = "source_type", length = 50)
	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	@Column(name = "race_code", length = 50)
	public String getRaceCode() {
		return raceCode;
	}

	public void setRaceCode(String raceCode) {
		this.raceCode = raceCode;
	}

	@Column(name = "distance_text", length = 50)
	public String getDistanceText() {
		return distanceText;
	}

	public void setDistanceText(String distanceText) {
		this.distanceText = distanceText;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		EventRace other = (EventRace) obj;

		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		return true;
	}

}
