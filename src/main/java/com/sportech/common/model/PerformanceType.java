package com.sportech.common.model;

public enum PerformanceType {
	 Matinee,
	 Twilight,
	 Evening,
	 Encore
}
