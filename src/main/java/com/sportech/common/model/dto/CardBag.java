package com.sportech.common.model.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.sportech.common.model.entity.EventCard;

public class CardBag implements Serializable {
    
    private static final long serialVersionUID = 1408083717495844823L;
    
    private Set<EventCard> cards = new HashSet<EventCard>();
    
    private Integer count;
    private String cardDate;
    
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getCardDate() {
        return cardDate;
    }

    public void setCardDate(String cardDate) {
        this.cardDate = cardDate;
    }

    public Set<EventCard> getCards() {
        return cards;
    }

    public void setCards(Set<EventCard> cards) {
        this.cards = cards;
    }
        
}
