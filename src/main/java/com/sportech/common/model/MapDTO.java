package com.sportech.common.model;

import java.util.Map;

public interface MapDTO {

    public Object getValue(String key);

    public void setValue(String key, Object value);

    public Map<String, Object> getValues();

}
