package com.sportech.common.util;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;

public class DateHelper {

    static private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    public static Date combine(Date date, Date time) {
        if (date == null || time == null) {
            return time;
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(time);
        Calendar result = Calendar.getInstance();
        result.set(Calendar.YEAR, cal1.get(Calendar.YEAR));
        result.set(Calendar.MONTH, cal1.get(Calendar.MONTH));
        result.set(Calendar.DAY_OF_MONTH, cal1.get(Calendar.DAY_OF_MONTH));
        result.set(Calendar.HOUR_OF_DAY, cal2.get(Calendar.HOUR_OF_DAY));
        result.set(Calendar.MINUTE, cal2.get(Calendar.MINUTE));
        result.set(Calendar.SECOND, cal2.get(Calendar.SECOND));
        return result.getTime();    
    }
    
    public static boolean isSameDay(Date d1, Date d2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(d1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(d2);
        if (cal1.get(Calendar.YEAR) != cal2.get(Calendar.YEAR)) {
            return false;
        }
        if (cal1.get(Calendar.MONTH) != cal2.get(Calendar.MONTH)) {
            return false;
        }
        if (cal1.get(Calendar.DAY_OF_MONTH) != cal2.get(Calendar.DAY_OF_MONTH)) {
            return false;
        }
        return true;
    }

    public static boolean isSameTime(Date d1, Date d2) {
        if (d1 == null || d2 == null) {
            return false;
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(d1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(d2);
        if (cal1.get(Calendar.HOUR_OF_DAY) != cal2.get(Calendar.HOUR_OF_DAY)) {
            return false;
        }
        if (cal1.get(Calendar.MINUTE) != cal2.get(Calendar.MINUTE)) {
            return false;
        }
        if (cal1.get(Calendar.SECOND) != cal2.get(Calendar.SECOND)) {
            return false;
        }
        return true;
    }

    public static Date getLocalTime(TimeZone timeZone, Date utc) {
        if (utc == null) {
            return null;
        }
        int offset = timeZone.getOffset(new Date().getTime());
        return new Date(utc.getTime() + offset);
    }

    public static Date getUTCTime(TimeZone timeZone, Date local) {
        if (local == null) {
            return null;
        }
        int offset = timeZone.getOffset(new Date().getTime());
        return new Date(local.getTime() - offset);
    }

    public static Date getCardDate(XMLGregorianCalendar c) {
        if (c == null) {
            return null;
        }
        GregorianCalendar g = new GregorianCalendar();
        g.set(c.getYear(), c.getMonth() - 1, c.getDay(), 0, 0);
        return g.getTime();
    }

    public static Date getPostTime(XMLGregorianCalendar c) {
        if (c == null) {
            return null;
        }
        GregorianCalendar g = new GregorianCalendar();
        g.set(0, 0, 0, c.getHour(), c.getMinute(), 0);
        return g.getTime();
    }

    public static String dateToStr(String format, Date d) {
        if (d == null) {
            return "";
        }
        SimpleDateFormat sf = new SimpleDateFormat(format);
        return sf.format(d);
    }

    public static String dateToStr(Date d) {
        if (d == null) {
            return "";
        }
        return sdf.format(d);
    }

    public static Date stringToDate(String s) {
        return stringToDate(sdf, s);
    }

    public static Date stringToDate(SimpleDateFormat f, String s) {
        Date d = null;
        if (f != null && !StringUtils.isBlank(s)) {
            try {
                d = f.parse(s);
            } catch (Exception e) {
                
            }
        }
        return d;
    }

}
