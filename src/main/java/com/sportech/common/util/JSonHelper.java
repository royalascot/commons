package com.sportech.common.util;

import java.io.StringWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

public class JSonHelper {

    static private final Logger log = Logger.getLogger(JSonHelper.class);

    public static <T> T fromJSon(Class<T> c, String s) {
        if (StringUtils.isEmpty(s)) {
            return null;
        }
        try {
            ObjectMapper jsonMapper = new ObjectMapper();
            T object = jsonMapper.readValue(s, c);
            return object;
        } catch (Exception e) {
            log.error("Error create object from JSon string", e);
        }
        return null;
    }

    public static <T> String toJSon(T t) {
        if (t == null) {
            return null;
        }
        try {
            StringWriter sw = new StringWriter();
            ObjectMapper jsonMapper = new ObjectMapper();
            jsonMapper.writeValue(sw, t);
            return sw.toString();
        } catch (Exception e) {
            log.error("Error create JSon string", e);
        }
        return null;
    }

}
