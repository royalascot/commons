package com.sportech.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

public class ListHelper {

    public static Set<String> parse(String leg) {
        Set<String> l = new HashSet<String>();
        if (StringUtils.isBlank(leg)) {
            return l;
        }
        StringTokenizer st = new StringTokenizer(leg, "-,", true);
        int start = -1;
        int last = -1;
        while (st.hasMoreTokens()) {
            String s = st.nextToken();
            s = s.trim();
            if ("-".equals(s)) {
                if (start == -2) {
                    return null;
                }
                start = last;
                continue;
            }
            if (",".equals(s)) {
                start = -1;
                continue;
            }
            if (StringUtils.isNumeric(s)) {
                int i = Integer.parseInt(s);
                if (start > 0) {
                    if (i <= start) {
                        return null;
                    }
                    for (int j = start + 1; j <= i; j++) {
                        l.add("" + j);
                    }
                    start = -1;
                } else {
                    start = -1;
                    l.add(s.trim());
                    last = i;
                }
            } else {
                if (start > 0) {
                    return null;
                }
                start = -2;
                l.add(s.trim());
            }
        }
        return l;
    }

    public static Set<Long> parseList(String leg) {
        Set<Long> l = new HashSet<Long>();
        if (StringUtils.isBlank(leg)) {
            return l;
        }
        StringTokenizer st = new StringTokenizer(leg, "-,", true);
        int start = -1;
        int last = -1;
        while (st.hasMoreTokens()) {
            String s = st.nextToken();
            s = s.trim();
            if ("-".equals(s)) {
                if (start == -2) {
                    return null;
                }
                start = last;
                continue;
            }
            if (",".equals(s)) {
                start = -1;
                continue;
            }
            if (StringUtils.isNumeric(s)) {
                int i = Integer.parseInt(s);
                if (start > 0) {
                    if (i <= start) {
                        return null;
                    }
                    for (int j = start + 1; j <= i; j++) {
                        l.add(new Long(j));
                    }
                    start = -1;
                } else {
                    start = -1;
                    l.add(Long.parseLong(s.trim()));
                    last = i;
                }
            } else {
                if (start > 0) {
                    return null;
                }
                start = -2;
                l.add(Long.parseLong(s.trim()));
            }
        }
        return l;
    }

    public static String build(Collection<Long> items) {
        List<Long> sorted = new ArrayList<Long>();
        sorted.addAll(items);
        Collections.sort(sorted);
        List<String> results = new ArrayList<String>();
        long start = -1;
        long end = -1;
        String current = null;
        for (Long i : sorted) {
            if (current == null) {
                start = i;
                end = i;
                current = "" + i;
            } else {
                if (i == end + 1) {
                    end = i;
                } else {
                    if (start != end) {
                        current = current + "-" + end;
                    }
                    results.add(current);
                    current = "" + i;
                    start = i;
                    end = i;
                }
            }
        }
        if (current != null) {
            if (start != end) {
                current = current + "-" + end;
            }
            results.add(current);
        }
        return StringUtils.join(results, ",");
    }

    public static Collection<Long> parseRaceList(String raceList) {
        Set<Long> races = new HashSet<Long>();
        if (!StringUtils.isBlank(raceList)) {
            String[] legs = raceList.split(",");
            for (String l : legs) {
                String[] range = l.split("-");
                long start = Long.parseLong(StringUtils.trim(range[0]));
                long end = start;
                if (range.length > 1) {
                    end = Long.parseLong(StringUtils.trim(range[1]));
                }
                for (long r = start; r <= end; r++) {
                    races.add(r);
                }
            }
        }
        return races;
    }

}
