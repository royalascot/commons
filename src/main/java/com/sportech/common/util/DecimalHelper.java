package com.sportech.common.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.apache.log4j.Logger;

public class DecimalHelper {

	static private final Logger log = Logger.getLogger(DecimalHelper.class);
	
	public static BigDecimal toCents(BigDecimal dollar) {
		if (dollar == null) {
			return null;
		}
		return dollar.movePointRight(2);
	}

	public static BigDecimal fromCents(Integer cents) {
		if (cents == null) {
			return null;
		}
		return fromCents(new BigDecimal(cents));
	}
    
    public static BigDecimal fromInteger(Integer value, int scale) {
        if (value == null) {
            return null;
        }
        BigDecimal v = new BigDecimal(value.intValue());
        if (scale != 0) {
            v = v.movePointLeft(scale);
        }
        return v;
    }
    
	public static boolean areEqual(BigDecimal b1, BigDecimal b2) {
		if (b1 == null || b2 == null) {
			return false;
		}
		return (b1.compareTo(b2)) == 0;
	}

	private static BigDecimal fromCents(BigDecimal cents) {
		if (cents == null) {
			return null;
		}
		return cents.movePointLeft(2);
	}

	public static BigDecimal parseAmount(String s) {
		BigDecimal amount = null;
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator(',');
		symbols.setDecimalSeparator('.');
		String pattern = "#,##0.0#";
		DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
		decimalFormat.setParseBigDecimal(true);
		try {
			amount = (BigDecimal) decimalFormat.parse(s);
		} catch (Exception e) {
			log.error("Error parsing amount string:" + s, e);
		}
		return amount;
	}
	
	public static Long parseAmountIntoPennies(String s) {
		BigDecimal b = parseAmount(s);
		if (b != null) {
			return b.movePointRight(2).longValue();
		}
		return null;
	}
	
}