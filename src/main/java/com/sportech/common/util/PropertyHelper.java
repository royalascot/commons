package com.sportech.common.util;

import java.beans.PropertyDescriptor;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import javax.naming.InitialContext;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

public class PropertyHelper {

    static private final Logger log = Logger.getLogger(PropertyHelper.class);

    @SuppressWarnings("serial")
    static private final HashSet<Class<?>> supportTypes = new HashSet<Class<?>>() {
        {
            add(String.class);
            add(Integer.class);
            add(Long.class);
            add(Date.class);
        }
    };

    @SuppressWarnings("rawtypes")
    public static <T> void populateNonNullValues(T dest, T src) {
        try {
            Map map = BeanUtils.describe(src);
            for (Object o : map.keySet()) {
                String k = (String) o;
                PropertyDescriptor pd = PropertyUtils.getPropertyDescriptor(src, k);
                if (supportTypes.contains(pd.getPropertyType())) {
                    Object value = PropertyUtils.getProperty(src, k);
                    if (value != null) {
                        PropertyUtils.setProperty(dest, k, value);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error processing populateNonNullValues()", e);
        }
    }
    
    public static void copyProperties(Object dest, Object src) {
        try {
            PropertyUtils.copyProperties(dest, src);
        } catch (Exception e) {
            log.error("Error copying properties", e);
        }
    }
    
    public static String getJndiProperty(String key) {
        String prop = null;
        try {
            prop = InitialContext.doLookup(key);
        } catch (Exception e) {
            log.error("Can not find JNDI property:" + key);
        }
        return prop;
    }
    
}