package com.sportech.common.util;

import java.io.ByteArrayInputStream;
import java.util.zip.DeflaterInputStream;
import java.util.zip.InflaterInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

public class ZipHelper {

	static private final Logger log = Logger.getLogger(ZipHelper.class);
	
	public static byte[] inflateBuffer(byte[] data) {
        byte[] theData = null;
        ByteArrayInputStream fis = null;
        InflaterInputStream zis = null;
        try {
            fis = new ByteArrayInputStream(data);
            zis = new InflaterInputStream(fis);
            theData = IOUtils.toByteArray(zis);
        } catch (Exception e) {
            log.error("Error extracting compressed data stream", e);
        } finally {
            IOUtils.closeQuietly(zis);
            IOUtils.closeQuietly(fis);
        }
        return theData;
    }

    public static byte[] deflateBuffer(byte[] data) {
        byte[] theData = null;
        ByteArrayInputStream fis = null;
        DeflaterInputStream zis = null;
        try {
            fis = new ByteArrayInputStream(data);
            zis = new DeflaterInputStream(fis);
            theData = IOUtils.toByteArray(zis);
        } catch (Exception e) {
            log.error("Error compressing data stream", e);
        } finally {
            IOUtils.closeQuietly(zis);
            IOUtils.closeQuietly(fis);
        }
        return theData;
    }

}
