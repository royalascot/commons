package com.sportech.common.actionrule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.reflections.Reflections;

public class RuleExecutor<T extends ProcessingRule> {

    static private final Logger log = Logger.getLogger(RuleExecutor.class);

    private class RuleComparator implements Comparator<ProcessingRule> {

        @Override
        public int compare(ProcessingRule p1, ProcessingRule p2) {
            if (p1 == null || p2 == null) {
                return -1;
            }
            int r1 = p1.getPriority();
            int r2 = p2.getPriority();
            return r1 - r2;
        }

    }

    private List<T> rules = new ArrayList<T>();

    public RuleExecutor(String packageName, Class<T> clazz) {
        init(packageName, clazz);
    }

    public int execute(RuleContext context) {
        int count = 0;
        try {
            for (ProcessingRule rule : rules) {
                if (rule.isApplicable(context)) {
                    count++;
                    if (!rule.process(context)) {
                        break;
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error execute rules", e);
        }
        return count;
    }

    private void init(String packageName, Class<T> clazz) {
        Reflections reflections = new Reflections(packageName);
        Set<Class<? extends T>> classes = reflections.getSubTypesOf(clazz);
        try {
            for (Class<? extends T> c : classes) {
                T rule = c.getConstructor().newInstance();
                rules.add(rule);
            }
            Collections.sort(rules, new RuleComparator());
        } catch (Exception e) {
            log.error("Error creating rule objects", e);
        }
    }

}
