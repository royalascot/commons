package com.sportech.common.actionrule;


public interface ProcessingRule {

    public int getPriority();
    
    public boolean isApplicable(RuleContext context);
    
    public boolean process(RuleContext context);

}
