package com.sportech.common.util;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

public class DecimalHelperTest {
    
	@Test
    public void testFromCents() {
		BigDecimal result = DecimalHelper.fromCents(null);
		Assert.assertTrue(result == null);
		result = DecimalHelper.fromCents(100);
		Assert.assertTrue(DecimalHelper.areEqual(BigDecimal.ONE, result));
    }

    @Test
    public void testToCents() {
    	BigDecimal result = DecimalHelper.fromCents(100);
		Assert.assertTrue(DecimalHelper.areEqual(BigDecimal.ONE, result));
		result = DecimalHelper.toCents(result);
		Assert.assertTrue(result.intValue() == 100);
		Assert.assertTrue(DecimalHelper.areEqual(new BigDecimal(100), result));
    }
    
    @Test
    public void testToAndFromCents() {    	
    	BigDecimal result = DecimalHelper.fromCents(12345);
		Assert.assertTrue(DecimalHelper.areEqual(new BigDecimal("123.45"), result));
		result = DecimalHelper.toCents(result);
		Assert.assertTrue(DecimalHelper.areEqual(new BigDecimal(12345), result));
    }

    @Test
    public void testAmountParsing() {
    	BigDecimal result = DecimalHelper.parseAmount("123456.789");    	
		Assert.assertTrue(DecimalHelper.areEqual(BigDecimal.valueOf(123456789L).movePointLeft(3), result));
		Long l = DecimalHelper.parseAmountIntoPennies("123456.78");
		Assert.assertTrue(l == 12345678);
		l = DecimalHelper.parseAmountIntoPennies("123,456.78");
		Assert.assertTrue(l == 12345678);
		l = DecimalHelper.parseAmountIntoPennies("0.78");
		Assert.assertTrue(l == 78);
		l = DecimalHelper.parseAmountIntoPennies("0.78000");
		Assert.assertTrue(l == 78);
		l = DecimalHelper.parseAmountIntoPennies("0.78999");
		Assert.assertTrue(l == 78);
    }

}
