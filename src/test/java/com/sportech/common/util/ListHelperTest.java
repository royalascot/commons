package com.sportech.common.util;

import java.util.Arrays;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import com.sportech.common.util.ListHelper;

public class ListHelperTest {

    @Test
    public void testParsingRange1() {
        Set<Long> s = ListHelper.parseList("1,2,3,5-7");
        Assert.assertTrue(!s.contains(4L));
        Assert.assertTrue(s.contains(1L));
        Assert.assertTrue(s.contains(2L));
        Assert.assertTrue(s.contains(3L));
        Assert.assertTrue(s.contains(5L));
        Assert.assertTrue(s.contains(6L));
        Assert.assertTrue(s.contains(7L));
    }

    @Test
    public void testBuilding() {
        Long[] list = new Long[] { 1l, 3l, 5l, 4l, 7l, 8l, 9l };
        String result = ListHelper.build(Arrays.asList(list));
        Assert.assertTrue(StringUtils.equals(result, "1,3-5,7-9"));
    }

    @Test
    public void testBuilding1() {
        Long[] list = new Long[] { 1l };
        String result = ListHelper.build(Arrays.asList(list));
        Assert.assertTrue(StringUtils.equals(result, "1"));
    }

    @Test
    public void testBuilding2() {
        Long[] list = new Long[] {};
        String result = ListHelper.build(Arrays.asList(list));
        Assert.assertTrue(StringUtils.equals(result, ""));
    }

    @Test
    public void testBuilding3() {
        Long[] list = new Long[] { 1l, 3l, 2l, 5l, 4l, 6l, 7l, 8l, 9l };
        String result = ListHelper.build(Arrays.asList(list));
        Assert.assertTrue(StringUtils.equals(result, "1-9"));
    }

    @Test
    public void testBuilding4() {
        Long[] list = new Long[] { 1l, 9l };
        String result = ListHelper.build(Arrays.asList(list));
        Assert.assertTrue(StringUtils.equals(result, "1,9"));
    }

}
