package com.sportech.common.util;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

public class ZipHelperTest {

	@Test
	public void testRandom() {
		int size = 10000;
		byte[] source = new byte[size];
		Random r = new Random();
		r.nextBytes(source);
		byte[] result = ZipHelper.deflateBuffer(source);
		byte[] buffer = ZipHelper.inflateBuffer(result);
		for (int i = 0; i < size; i++) {
			Assert.assertTrue(buffer[i] == source[i]);
		}
	}

	@Test
	public void testSequence() {
		int size = 20000;
		byte[] source = new byte[size];
		for (int i = 0; i < size; i++) {
			source[i] = (byte) i;
		}
		byte[] result = ZipHelper.deflateBuffer(source);
		byte[] buffer = ZipHelper.inflateBuffer(result);
		Assert.assertTrue(result.length < source.length);
		for (int i = 0; i < size; i++) {
			Assert.assertTrue(buffer[i] == source[i]);
		}
	}

}
